package com.example.async_project;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity {
    EditText numApostas, resultado;
    ProgressBar barra;

    Handler hdlr;

    ExecutorService exc = Executors.newSingleThreadExecutor();
    GeradorNumeros geradorNumeros;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        numApostas = (EditText) findViewById(R.id.edNumApostas);
        resultado = (EditText) findViewById(R.id.edResultado);
        barra = (ProgressBar)  findViewById(R.id.barraProgresso);

        hdlr = new Handler(Looper.getMainLooper());
    }

    public void gerar(View v) {
        try {
            int apostas = Integer.parseInt(numApostas.getText().toString());
            barra.setMax(apostas);
            barra.setProgress(0);
            geradorNumeros = new GeradorNumeros(apostas);
            exc.execute(geradorNumeros);
        }
        catch (NumberFormatException ex) {
            Toast.makeText(this, "Apenas numeros", Toast.LENGTH_SHORT).show();
        }
    }

    public void cancelar(View view) {
        if(geradorNumeros != null) {
            geradorNumeros.cancelar();
            geradorNumeros = null;
            barra.setProgress(0);
        }
    }

    class GeradorNumeros implements Runnable {
        int numApostas;
        boolean cancelado = false;

        public GeradorNumeros(int apostas) {
            numApostas = apostas;
        }

        public void cancelar() {
            cancelado = true;
        }

        @Override
        public void run() {
            Random r = new Random(System.currentTimeMillis());
            try {
                for (int i = 0; i < numApostas; i++) {
                    String aposta = "";
                    for (int j = 0; j < 6; j++) {
                        if(cancelado) {
                            return;
                        }

                        int num = r.nextInt(60) + 1;
                        aposta += num + " ";
                        Thread.sleep(250);
                    }

                    final String linha = aposta + "\n";
                    final int progresso = i + 1;

                    hdlr.post(new Runnable() {
                        @Override
                        public void run() {
                            resultado.append(linha);
                            barra.setProgress(progresso);
                        }
                    });
                    Thread.sleep(500);
                }
            }catch (InterruptedException t){
                System.out.println(t.getMessage());
            }
        }
    }
}